import locators from "../support/locators"

describe('Suite de teste de movimentação de estoque', ()=> {
  beforeEach(() => {
    cy.login()
  })
  it('Insere novo item no estoque', function ()  {
    cy.inserirVeiculo()

    cy.get('.Toastify').should('contain', 'Carro cadastrado')
    cy.url().should('eq', 'http://carshop.kf.avanz.com.br/gerenciar-estoque')
    cy.get(locators.GERENCIARESTOQUE.LISTAESTOQUE)
      .scrollTo('bottom'); 
    cy.get('div.car-item.position-relative.bg-white.rounded.d-flex.gap-3')
      .should('be.visible')
      .contains('.car-name.text-uppercase.fw-semibold', 'X6')
      .should('be.visible')
    cy.get('div.d-flex.flex-column.align-items-start.gap-3.flex-md-row.align-items-md-center')
      .should('be.visible')
    cy.get('p.car-model.text-uppercase.fw-semibold')
      .should('be.visible')
      .and('contain.text', 'BMW');
  })

  it('Deleta o item do estoque', () => {
    cy.visit('./gerenciar-estoque')
    cy.get('#X6-BMW').check()
    cy.get(locators.GERENCIARESTOQUE.BOTAOEXCLUIR).click()
    cy.get(locators.GERENCIARESTOQUE.MODALCONFIRMACAOEXCLUSAO)
      .should('be.visible')
    .within(() => {
      cy.get(locators.GERENCIARESTOQUE.BOTAOCONFIRMAR)
      .should('be.visible')
      .click();
    });

    cy.get('.Toastify').should('contain', 'Carro deletado')
  })
})

