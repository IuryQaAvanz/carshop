import locators from "../support/locators"
import 'cypress-real-events/support'
const faker = require('faker-br');

describe('Suite de testes de gerenciamento de usuários', () => {
  beforeEach(() => {
        cy.login()
        cy.clearCookies()
  })

  const nome = faker.name.firstName().replace(/[^´a-zA-Z\s]/g, '')
  const sobrenome = faker.name.lastName().replace(/[^´a-zA-Z\s]/g, '')
  const username = `${nome}${sobrenome}`
  const email = `${nome}${sobrenome}@dominio.com`
  const cpf = faker.br.cpf()
  const telefone1 = faker.phone.phoneNumber();
  const telefone2 = faker.phone.phoneNumber();

  it('Adiciona um novo usuário.', () => {
    cy.visit('./dashboard')
    cy.get('button:contains("Conta")').first().click();
    cy.get('a:contains("Usuários")').first().click();
    cy.get('button:contains("+ Adicionar usuário")').click();
    cy.get(locators.GERENCIARUSUARIOS.NOME).type(nome)
    cy.get(locators.GERENCIARUSUARIOS.SOBRENOME).type(sobrenome)
    cy.get(locators.GERENCIARUSUARIOS.USERNAME).type(username.toLowerCase())
    cy.get(locators.GERENCIARUSUARIOS.CPF).type(cpf)
    cy.get(locators.GERENCIARUSUARIOS.EMAIL).type(email.toLowerCase())
    cy.get(locators.GERENCIARUSUARIOS.TELEFONE1).type(telefone1)
    cy.get(locators.GERENCIARUSUARIOS.TELEFONE2).type(telefone2)
    cy.get(locators.GERENCIARUSUARIOS.CARGOADMIN).click()
    cy.get(locators.GERENCIARUSUARIOS.SENHA).eq(0).type('12@A56b!')
    cy.get(locators.GERENCIARUSUARIOS.SENHA).eq(1).type('12@A56b!')
    cy.get(locators.GERENCIARUSUARIOS.ADICIONARFOTO)
      .selectFile('cypress/fixtures/imagens/fotoNovoUsuario.jpg', {force:true})
      .then(input => {
        expect(input[0].files[0].name).to.equal('fotoNovoUsuario.jpg')
    })
    cy.get(locators.GERENCIARUSUARIOS.BTNADICIONAR).click()

    cy.get(locators.GERENCIARUSUARIOS.NOTIFICACAO).should('contain', 'Usuario cadastrado com sucesso!')
    cy.contains(username.toLowerCase()).should('exist')
  })

  it('Edita o usuário criado.', () => {
    cy.visit('./usuarios');
    cy.get('tr').contains(username.toLowerCase()).parent().find('button[id="dropdown-basic"]').click({force: true});
    cy.get('a.dropdown-item:contains("Editar")', { timeout: 60000 }).as('editLink');
    cy.get('@editLink').should('be.visible')
      .then((element) => {
      cy.wrap(element).click({force: true});
    });
    cy.wait(2000);
    cy.get('[name="cel_phone"]').clear().type(telefone2);
    cy.get('[type="submit"]').click();
  
    cy.get(locators.GERENCIARUSUARIOS.NOTIFICACAO).should('contain', 'Usuario atualizado com sucesso!');
  });

  it('Exclui o usuário criado.', () => {
    cy.visit('./usuarios');
    cy.get('tr').contains(username.toLowerCase()).parent().as('userRow');
    cy.get('@userRow').trigger('mouseover', {force: true});
    
    cy.wait(500);
    cy.get('@userRow').find('button[id="dropdown-basic"]').click({force: true});
    cy.get('@userRow').find('a.dropdown-item:contains("Excluir")', { timeout: 60000 }).as('deleteLink')
      .should('be.visible')
      .then((element) => {
        
        cy.wrap(element).invoke('show').click({ force: true });
      });
    cy.wait(2000);
  
    cy.get('button:contains("Confirmar")').as('confirmButton')
    cy.get('@confirmButton').click({force: true});
    
    cy.get(locators.GERENCIARUSUARIOS.NOTIFICACAO).should('contain', 'Usuário apagado com sucesso!');
  });
  
})