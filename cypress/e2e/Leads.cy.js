import locators from "../support/locators"
import 'cypress-real-events/support'
const faker = require('faker-br');

describe('Suite de testes de gerenciamento de leads', () => {
  beforeEach(() => {
        cy.login()
  })
  
  it('Insere novo Lead', () => {
    cy.CadastraNovoLead()

    cy.get(locators.GERENCIARLEADS.NOTIFICACAO).should('contain', 'Lead cadastrado com sucesso!')
  })
 
  it('Edita usuário', () => {
    cy.EditaOUsuarioLead()

    cy.get(locators.GERENCIARLEADS.NOTIFICACAO).should('contain', 'Cadastro atualizado')
  })
  
  //TODO...
  it.skip('Atualiza status do lead, com excessão de venda realizada', () => {})

})