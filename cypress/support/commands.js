import locators from "../support/locators"
const faker = require('faker-br');
const moment = require('moment')
const accessToken = `Bearer ${Cypress.env('token')}`

Cypress.Commands.add('Authorization', () => {
  cy.api({
    method: 'POST',
    url: '/login', 
    headers: { Authorization: accessToken },

    }).then((response) => {
    expect(response.status).to.eq(200)
   })
})

Cypress.Commands.add('login', (
    usuario = Cypress.env('usuario'),
    senha = Cypress.env('senha'),
    { cacheSession = true } = {},
) => {
  const login = () => {
    cy.visit('./login');

    cy.get('input[name="username"]').type(usuario)
    cy.get('input[name="password"]').type(senha, { log:false} )  
    cy.get('button[type="submit"]').click()
    cy.get('.Toastify').should('contain', 'Bem vindo')
    cy.url().should('eq', 'http://carshop.kf.avanz.com.br/dashboard')
  }
  
  const validate = () => {
    cy.visit('./')
    cy.location('pathname', { timeout: 1000 })
      .should('not.eq', './login')
  }

  const options = {
    cacheAcrossSpecs: true,
    validate,
  }

  if (cacheSession) {
    cy.session(usuario, login, options)
  } else {
    cy.api({
        method: 'POST',
        url: '/login', 
        headers: { Authorization: accessToken },
    
        }).then((response) => {
        expect(response.status).to.eq(200)
       })
    
    login()
    //cy.clearAllCookies()
  }
})

Cypress.Commands.add('inserirVeiculo', function () {
  cy.visit('./dashboard')
  cy.clearAllCookies()
  cy.get(locators.GERENCIARESTOQUE.MENULATERALGE).eq(0).click({force: true});
  cy.get(locators.GERENCIARESTOQUE.BOTAOADICIONARVEICULO).click()
  cy.get(locators.GERENCIARESTOQUE.INFORMARDADOSMANUALMENTE).click()
  cy.fixture("gerenciarEstoque").as('itemForm').then(() => {
    cy.get(locators.GERENCIARESTOQUE.TIPODEVEICULOCARRO).click({force:true})
    cy.get(locators.GERENCIARESTOQUE.MARCA).type(this.itemForm.marca)
    cy.get(locators.GERENCIARESTOQUE.MODELO).type(this.itemForm.modelo);
    cy.get(locators.GERENCIARESTOQUE.ANOMODELO).type(this.itemForm.anomodelo);
    cy.get(locators.GERENCIARESTOQUE.ANOFABRICACAO).type(this.itemForm.anofabricacao);
    cy.get(locators.GERENCIARESTOQUE.VERSAO).type(this.itemForm.versao);
    cy.get(locators.GERENCIARESTOQUE.PORTAS).select('5');
    cy.get(locators.GERENCIARESTOQUE.COR).type(this.itemForm.cor);
    cy.get(locators.GERENCIARESTOQUE.CAMBIO).type(this.itemForm.cambio);
    cy.get(locators.GERENCIARESTOQUE.TIPODECOMBUSTIVEL).type(this.itemForm.tipodecombustivel);
    cy.get(locators.GERENCIARESTOQUE.MOTOR).type(this.itemForm.motor);
    cy.get(locators.GERENCIARESTOQUE.PLACA).type(this.itemForm.placa);
    cy.get(locators.GERENCIARESTOQUE.DISTANCIAPERCORRIDA).type(this.itemForm.distanciapercorrida);
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL01ARDIGITAL).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL02CONTROLEDEVELOCIDADE).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL03VONTALNTECOMREGULAGEM).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL04GARANTIADEFABRICA).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL05LICENCIAMENTO).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL06IPVAPAGO).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL07UNICODONO).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL08BANCOBIPARTIDO).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL09BANCOCOMAJUSTE).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL10ENCOSTODECABECA).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL11ASSISTENTEDEPARTIDAEMRAMPA).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL12RODASLIGALEVE).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL13TETOSOLAR).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL14FAROLDENEBLINA).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL15AIRBAGDUPLO).click()
    cy.get(locators.GERENCIARESTOQUE.OPCIONAL16ALARME).click()
    cy.get(locators.GERENCIARESTOQUE.CATEGORIA).type(this.itemForm.categoria);
    cy.get(locators.GERENCIARESTOQUE.BOTAOADICIONAR).click()      
    cy.get(locators.GERENCIARESTOQUE.OBSERVACOES).type(this.itemForm.observacoes)
    cy.get(locators.GERENCIARESTOQUE.BOTAOSELECIONEOARQUIVO)
      .selectFile('cypress/fixtures/imagens/BMW-X6.jpg', { force: true })
      .then(input => {
        expect(input[0].files[0].name).to.equal('BMW-X6.jpg')
      })
    cy.get(locators.GERENCIARESTOQUE.PRECOCOMPRA).type(this.itemForm.valorcompra)
    cy.get(locators.GERENCIARESTOQUE.PRECOVENDA).type(this.itemForm.valorvenda)
    cy.get(locators.GERENCIARESTOQUE.VIDEOURL).type(this.itemForm.videourl)
  })
  cy.get(locators.GERENCIARESTOQUE.BOTAOINCLUIRVEICULO).click({ force: true })
})

Cypress.Commands.add('selecionaMarcadorAleatorio', () => {
  cy.get('select[name="contacts.0.marcador"]')
    .find('option')
    .then(options => {
      const randomIndex = Math.floor(Math.random() * options.length);
      cy.get('select[name="contacts.0.marcador"]').select(options[randomIndex].value)
    })
})

Cypress.Commands.add('SelecionaProdutoAleatorio', () => {
  cy.get('select[name="product_id"]')
    .find('option')
    .then(options => {
      const randomIndex = Math.floor(Math.random() * options.length);
      cy.get('select[name="product_id"]').select(options[randomIndex].value)
    })
})

Cypress.Commands.add('SelecionaOrigemLeadAleatorio', () => {
  cy.get('select[name="originLead"]')
    .find('option')
    .then(options => {
      const randomIndex = Math.floor(Math.random() * options.length);
      cy.get('select[name="originLead"]').select(options[randomIndex].value)
    })
})

Cypress.Commands.add('CadastraNovoLead', () => {
  const nomeCompleto = faker.name.findName().replace(/[^´a-zA-Z\s]/g, '');
  const [primeiroNome, segundoNome] = nomeCompleto.split(' ');
  const dominio = faker.internet.domainName();
  const email = faker.internet.userName(primeiroNome, segundoNome) + '@' + dominio;
  const telefone = faker.phone.phoneNumber()
  const cpf = faker.br.cpf();

  cy.visit('./gerenciar-leads')
  cy.get(locators.GERENCIARLEADS.MENULATERALLEADS).eq(0).click({force: true})
  cy.get(locators.GERENCIARLEADS.BOTAOCIRARNOVOLEAD).click()
  cy.get(locators.GERENCIARLEADS.FORMADICIONARNOVOLEAD).within(() => {
    cy.get(locators.GERENCIARLEADS.INPUTNOME).type(nomeCompleto); 
    cy.get(locators.GERENCIARLEADS.INPUTEMAIL).type(email.toLowerCase());
    cy.get(locators.GERENCIARLEADS.INPUTTELEFONE).type(telefone)
    cy.selecionaMarcadorAleatorio() 
    cy.get(locators.GERENCIARLEADS.INPUTCPF).type(cpf); 
    cy.SelecionaProdutoAleatorio()
    cy.SelecionaOrigemLeadAleatorio()
    cy.get(locators.GERENCIARLEADS.BOTAOADICIONARLEAD).click()
  });
})

Cypress.Commands.add('selecionaTipoContatoAleatorio', () => {
  ['type_contact', 'type_contact2'].forEach((contactType) => {
    cy.get(`select[name="${contactType}"]`)
      .find('option')
      .then(options => {
        const randomIndex = Math.floor(Math.random() * options.length);
        cy.get(`select[name="${contactType}"]`).select(options[randomIndex].value)
      })
  });
})

Cypress.Commands.add('digitaCEPAleatorio', (selector) => {
  const geraCEPAleatorio = () => {
    // Lista de faixas de possíveis CEPs válidos para o estado do Ceará
    const faixasCEP = ['60000-000', '63990-000'];
    const cep = faixasCEP[Math.floor(Math.random() * faixasCEP.length)];
    return cep;
  }

  const cep = geraCEPAleatorio();
  cy.get(selector).type(cep);
});


Cypress.Commands.add('EditaOUsuarioLead', () => {
  const telefone = faker.phone.phoneNumber()
  const dataDeNascimento = faker.date.past();
  const dataDeNascimentoFormatada = moment(dataDeNascimento).format('YYYY-MM-DD')
  const endereco = faker.address.streetName();
  const enderecoNumero = faker.random.number();
  const complemento = faker.address.secondaryAddress();
  const bairro = faker.address.county();

  cy.visit('./gerenciar-leads')
  cy.get(locators.GERENCIARLEADS.MENULATERALLEADS).eq(0).click({force: true})
  cy.get('._board-card_128og_1 ._board-card-heading_128og_11').first().click({force: true})
  cy.get('button._button_1tlnx_1:contains("Editar usuário")').should('exist').click();
  cy.get('input[name="cel_phone"]').clear().type(telefone)
  cy.get('input[name="cel_phone2"]').clear().type(telefone)
  cy.selecionaTipoContatoAleatorio()
  cy.digitaCEPAleatorio('input[name="cep"]');
  cy.get('input[name="birth_date"]').clear().type(dataDeNascimentoFormatada);
  cy.get('input[name="address"]').clear().type(endereco);
  cy.get('input[name="number"]').clear().type(enderecoNumero);
  cy.get('input[name="complement"]').clear().type(complemento);
  cy.get('input[name="district"]').clear().type(bairro);
  cy.get('button._button_1tlnx_1:contains("Atualizar cadastro")').click();
})

Cypress.Commands.add('selecionaStatusNegociacaoAleatorio', () => {
  cy.get('select#negociation_status')
    .find('option')
    .then(options => {
      // Comando para filtrar as opções e excluir "Venda realizada" ao selecionar...
      const validOptions = options.filter((_, option) => option.value !== 'was_converted');
      const randomIndex = Math.floor(Math.random() * validOptions.length);
      cy.get('select#negociation_status').select(validOptions[randomIndex].value)
    })
})
  