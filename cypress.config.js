const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    baseUrl: "http://carshop.kf.avanz.com.br/",
    video: true,
    defaultCommandTimeout: 30000,
    retries:3,
    setupNodeEvents(on, config) {
      require('@cypress/grep/src/plugin')(config)
      return config
    },
    experimentalRunAllSpecs: true,
    projectId: "csjk9t",
  },
});
